import React, { Component } from 'react'
import { connect } from 'react-redux'
import { XEM_CHI_TIET } from './redux/constants/shoeConstabts'

class Phanba extends Component {
  render() {
    return (
<div className="card" style={{width: '18rem'}}>
  <img className="card-img-top" src={this.props.dete.image} alt="Card image cap" />
  <div className="card-body">
    <h5 className="card-title">{this.props.dete.name}</h5>
    <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
   <button onClick={()=>{
    this.props.handleHang(this.props.dete)}} className='btn btn-warning'>Lên đơn</button>
   <button onClick={()=>{
    this.props.handleRender(this.props.dete)
   }} className='btn btn-success'>Xem chi tiet</button>
  </div>
</div>

    )
  }
}
let mapDispatchToProps=(dispatch)=>{
  return{
    handleRender: (value)=>{
      dispatch ({
        type: XEM_CHI_TIET,
        payload:value
      })
    }
  }
}

export default connect(null,mapDispatchToProps)(Phanba)
