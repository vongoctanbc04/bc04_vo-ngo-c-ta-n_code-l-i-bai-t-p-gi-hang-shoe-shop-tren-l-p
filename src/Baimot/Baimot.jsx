import React, { Component } from 'react'
import Giohang from './Giohang'
import Phanbon from './Phanbon'
import Phanhai from './Phanhai'
import { ngocTan } from './Phanmot'

export default class Baimot extends Component {
    state={
        shoeArr:ngocTan,
        age:ngocTan[0],
        gioHang:[],

    }
    // handleShow=(shoe)=>{
    //     this.setState({age:shoe})
    // }
    handleHang=(shoe)=>{
        let showHang=[...this.state.gioHang]
       let index = this.state.gioHang.findIndex((item)=>{
        return item.id == shoe.id;
       })
       if(index == -1){
        let spGioHang = {...shoe,soLuong:1}
        showHang.push(spGioHang);
       }else{ showHang[index].soLuong++
       }
       this.setState({gioHang:showHang})
    }
  render() {
    return (
      <div>
        <Giohang gioHang={this.state.gioHang} handleRemove={this.handleRemove}/>
        <Phanhai data={this.state.shoeArr}  handleHang={this.handleHang}/>
        <Phanbon />
      </div>
    )
  }
}
// alo={this.state.age}
