import { ngocTan } from "../../Phanmot";
import { XEM_CHI_TIET } from "../constants/shoeConstabts";

let initialState={
    shoeArr:ngocTan,
    age:ngocTan[0],
    gioHang:[],
};
export let shoeReducer=(state=initialState, action)=>{
    switch (action.type){
        case XEM_CHI_TIET:{
            state.age = action.payload;
            return {...state}
        }
        default:
            return state;
    }
};